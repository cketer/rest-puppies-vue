import Vue from 'vue'
import App from './App.vue'
import Add from './Add.vue'
import Puppies from './Puppies.vue'
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueAxios, axios);

Vue.component('puppies',Puppies);
Vue.component('add',Add);
new Vue({
  el: '#app',
  render: h => h(App)
})
